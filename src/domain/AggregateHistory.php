<?php

namespace Maybeway\Domain;


/**
 * Class AggregateHistory
 * @package Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class AggregateHistory extends DomainEvents
{
	/**
	 * @var IdentifiesAggregate
	 */
	private $aggregateId;

	/**
	 * @param IdentifiesAggregate $aggregateId
	 * @param array $events
	 * @throws \Exception
	 */
	public function __construct(IdentifiesAggregate $aggregateId, array $events)
	{
		if ( empty($events) ) {
			throw new EmptyAggregateHistory( $aggregateId );
		}

		/** @var $event DomainEvent */
		foreach($events as $event) {
			if (!$event->getAggregateId()->equals($aggregateId)) {
				throw new CorruptAggregateHistory("For aggregate id : {$aggregateId}");
			}
		}

		parent::__construct($events);
		$this->aggregateId = $aggregateId;
	}

	/**
	 * @return IdentifiesAggregate
	 */
	public function getAggregateId() : IdentifiesAggregate
	{
		return $this->aggregateId;
	}

	/**
	 * @param DomainEvent $domainEvent
	 * @throws \Exception
	 */
	public function append(DomainEvent $domainEvent)
	{
		throw new \Exception("@todo  Implement append() method.");
	}
}