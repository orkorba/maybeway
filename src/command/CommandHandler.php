<?php

namespace Maybeway\Command;

/**
 * Interface CommandHandler
 * @package Maybeway\Command
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface CommandHandler {}