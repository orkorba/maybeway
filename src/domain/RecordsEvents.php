<?php

namespace Maybeway\Domain;

/**
 * Interface RecordEvents
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface RecordsEvents
{
	/**
	 * @return DomainEvents
	 */
	public function getRecordedEvents() : DomainEvents;

	/**
	 * @return void
	 */
	public function clearRecordedEvents();

	/**
	 * @param AggregateHistory $aggregateHistory
	 * @return AggregateRoot
	 */
	public static function reconstituteFrom( AggregateHistory $aggregateHistory );
}