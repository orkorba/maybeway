<?php

namespace Maybeway\Event\SimpleEventBus;

use Maybeway\ClassProperties;
use Maybeway\Event\SimpleEventBus\Exception\MissingListenerClassInContainer;
use Psr\Container\ContainerInterface;

/**
 * Class SimpleEventBus
 * @package App\Model\SimpleEventBus
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class SimpleEventBus implements EventBus
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * @var array
	 */
	protected $listeners;

	/**
	 * @param ContainerInterface $container
	 * @param array $listeners
	 */
	public function __construct( ContainerInterface $container, array $listeners )
	{
		$this->container = $container;
		$this->listeners = $listeners;
	}

	/**
	 * @param DomainEvent $event
	 * @throws MissingListenerClassInContainer
	 */
	public function publish( DomainEvent $event )
	{
		$listeners = $this->findListenersForEvent( $event );

		foreach ( $listeners as $listener )
		{
			$this->dispatch( $listener, $event );
		}
	}

	/**
	 * @param $listener
	 * @param DomainEvent $event
	 * @throws MissingListenerClassInContainer
	 */
	public function dispatch( $listener, DomainEvent $event ) : void
	{
		$listener = lcfirst( $listener );
 		if ( $this->container->has( $listener ) )
		{
			$listenerClass = $this->container->get( $listener );
			$this->callMethod( $listenerClass, 'on'.ClassProperties::name( $event ), $event );
		}
		else
		{
 			throw new MissingListenerClassInContainer( $listener );
		}
	}

	/**
	 * @param $listenerClass
	 * @param $method
	 * @param DomainEvent $event
	 */
	protected function callMethod( $listenerClass, $method, DomainEvent $event )
	{
		if ( method_exists( $listenerClass, $method ) )
		{
			$listenerClass->{$method}( $event );
		}
	}

	/**
	 * @param DomainEvent $event
	 * @return array|mixed
	 */
	protected function findListenersForEvent( DomainEvent $event )
	{
		$eventClass = get_class( $event );

		if ( array_key_exists( $eventClass, $this->listeners ) ) {
			return $this->listeners[ $eventClass ];
		}
		return [];
	}
}