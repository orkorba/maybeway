<?php

namespace Maybeway\Command\SimpleCommandBus\Exception;

/**
 * Class CommandHandlerIsNotCallable
 * @package Maybeway\Command\SimpleCommandBus\Exception
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class CommandHandlerIsNotCallable extends \Exception {}