<?php

namespace Maybeway\Domain;

/**
 * Class EmptyAggregateHistory
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class EmptyAggregateHistory extends \Exception {}