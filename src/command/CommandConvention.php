<?php

namespace Maybeway\Command;

/**
 * Interface CommandConvention
 * @package Maybeway\Command
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface CommandConvention
{
	/**
	 * @param Command $command
	 * @return string
	 */
	public function handlerName( Command $command ) : string;
}