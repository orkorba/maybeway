<?php

namespace Maybeway\Domain;


/**
 * Interface AggregateRepository
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface AggregateRepository
{
	/**
	 * @param RecordsEvents $recordsEvents
	 * @return void
	 */
	public function save( RecordsEvents $recordsEvents );

	/**
	 * @param IdentifiesAggregate $identifiesAggregate
	 * @return AggregateRoot
	 */
	public function get( IdentifiesAggregate $identifiesAggregate ) : AggregateRoot;
}