<?php

namespace Maybeway\Domain;

/**
 * Interface IdentifiesAggregate
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface IdentifiesAggregate
{
	/**
	 * @param IdentifiesAggregate $other
	 * @return bool
	 */
	public function equals( IdentifiesAggregate $other ) : bool;
}