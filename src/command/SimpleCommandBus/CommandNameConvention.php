<?php

namespace Maybeway\Command\SimpleCommandBus;

use Maybeway\ClassProperties;
use Maybeway\Command\Command;
use Maybeway\Command\CommandConvention;


/**
 * Class CommandNameConvention
 * @package Maybeway\Command\SimpleCommandBus
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class CommandNameConvention implements CommandConvention
{

	/**
	 * @param Command $command
	 * @return string
	 */
	public function handlerName( Command $command ) : string
	{
		$className = lcfirst( ClassProperties::name( $command ) );

		return str_replace('Command', 'CommandHandler', $className );
	}

}