<?php

namespace Maybeway\Command;

/**
 * Interface Command
 * @package Maybeway\Comand
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface Command {}