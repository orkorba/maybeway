<?php

namespace Maybeway\Event;

use Maybeway\Domain\DomainEvent;

/**
 * Interface EventBus
 * @package App\Model
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface EventBus
{
	public function publish( DomainEvent $event );
}