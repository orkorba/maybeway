<?php

namespace Maybeway\Domain;

/**
 * Interface AggregateRoot
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface AggregateRoot
{
	/**
	 * @return IdentifiesAggregate
	 */
	public function getAggregateId() : IdentifiesAggregate;
}