<?php

namespace Maybeway;


/**
 * Class ClassProperties
 * @package Maybeway
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class ClassProperties
{
	/**
	 * @param $object
	 * @return string
	 */
	public static function name( $object ) : string
	{
		$objectFullName = is_object( $object ) ? get_class( $object ) : (string)$object;
		$parts = explode('\\', $objectFullName );
		return end( $parts );
	}
}