<?php

namespace Maybeway\Domain;

/**
 * Class DomainEvents
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class DomainEvents implements \Iterator
{
	protected $position;
	protected $events;

	public function __construct( array $events )
	{
		$this->events = $events;
		$this->position = 0;
	}

	public function rewind()
	{
		$this->position = 0;
	}

	public function current()
	{
		return $this->events[$this->position];
	}

	public function key() : int
	{
		return $this->position;
	}

	public function next() : int
	{
		++$this->position;
	}

	public function valid() : bool
	{
		return isset($this->events[$this->position]);
	}

}