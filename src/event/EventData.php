<?php

namespace Maybeway\Event;

use Maybeway\ClassProperties;
use Maybeway\Domain\DomainEvent;
use Maybeway\Domain\IdentifiesAggregate;

/**
 * Class EventData
 * @package Maybeway\Event
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class EventData
{
	protected $id;
	protected $aggregateId;
	protected $type;
	protected $version;
	protected $data;
	protected $createdAt;

	public function __construct( IdentifiesAggregate $aggregateId, $type, int $version, DomainEvent $event )
	{
		$this->id = Uuid::uuid4();
		$this->aggregateId = $aggregateId;
		$this->type = $type;
		$this->version = $version;
		$this->data = serialize( $event );
		$this->createdAt = $event->getOccurredAt();
	}

	public static function constructFromEvent( DomainEvent $event )
	{
		return new self(
			$event->getAggregateId(),
			ClassProperties::name( $event ),
			1,
			$event
		);
	}

	public function getEventId()
	{
		return $this->id;
	}

	/**
	 * @return IdentifiesAggregate
	 */
	public function getAggregateId(): IdentifiesAggregate
	{
		return $this->aggregateId;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreatedAt() : \DateTimeImmutable
	{
		return $this->createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return int
	 */
	public function getVersion() : int
	{
		return $this->version;
	}

	/**
	 * @return string
	 */
	public function getData() : string
	{
		return $this->data;
	}

	public function toArray() : array
	{
		return [
			'id' 			=> (string) $this->getEventId(),
			'aggregateId'	=> (string) $this->getAggregateId(),
			'data'			=> $this->getData(),
			'type' 			=> $this->getType(),
			'version'		=> $this->getVersion(),
			'createdAt'		=> $this->getCreatedAt()->format('Y-m-d H:i:s')
		];
	}
}