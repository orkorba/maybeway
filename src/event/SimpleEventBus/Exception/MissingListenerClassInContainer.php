<?php

namespace Maybeway\Event\SimpleEventBus\Exception;

/**
 * Class MissingListenerClassInContainer
 * @package Maybeway\Event\SimpleEventBus\Exception
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class MissingListenerClassInContainer extends \Exception {}