<?php

namespace Maybeway\Command;

/**
 * Interface CommandBus
 * @package Maybeway\Command
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface CommandBus
{
	/**
	 * @param Command $command
	 * @return void
	 */
	public  function execute( Command $command );
}