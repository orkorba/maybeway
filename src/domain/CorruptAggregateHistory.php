<?php

namespace Maybeway\Domain;

/**
 * Class CorruptAggregateHistory
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class CorruptAggregateHistory extends \Exception {}