<?php

namespace Maybeway\Event;

use Maybeway\Domain\AggregateHistory;
use Maybeway\Domain\DomainEvents;
use Maybeway\Domain\IdentifiesAggregate;


/**
 * Interface EventStore
 * @package Maybeway\Event
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface EventStore
{
	/**
	 * @param DomainEvents $domainEvents
	 * @return void
	 */
	public function commit( DomainEvents $domainEvents );

	/**
	 * @param IdentifiesAggregate $aggregateId
	 * @return AggregateHistory
	 */
	public function getAggregateHistoryFor( IdentifiesAggregate $aggregateId ) : AggregateHistory;
}