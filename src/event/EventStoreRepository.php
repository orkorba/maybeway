<?php

namespace Maybeway\Event;

use Maybeway\Domain\AggregateRoot;
use Maybeway\Domain\IdentifiesAggregate;

/**
 * Interface EventStoreRepository
 * @package App\Model
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface EventStoreRepository
{
	/**
	 * @param EventData $event
	 * @return void
	 */
	public function save( EventData $event );

	/**
	 * @param IdentifiesAggregate $aggregateId
	 * @return AggregateRoot
	 */
	public function get( IdentifiesAggregate $aggregateId ) : AggregateRoot;
}