<?php

namespace Maybeway\Command\SimpleCommandBus\Exception;

/**
 * Class CommandHasNoCorrespondsHandler
 * @package Maybeway\Command\SimpleCommandBus\Exception
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class CommandHasNoCorrespondsHandler extends \Exception {}