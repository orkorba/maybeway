<?php

namespace Maybeway\Domain;

/**
 * Interface DomainEvent
 * @package Maybeway\Domain
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
interface DomainEvent
{
	/**
	 * @return IdentifiesAggregate
	 */
	public function getAggregateId() : IdentifiesAggregate;

	/**
	 * @return \DateTimeImmutable
	 */
	public function getOccurredAt() : \DateTimeImmutable;
}