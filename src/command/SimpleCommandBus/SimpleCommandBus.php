<?php

namespace Maybeway\Command\SimpleCommandBus;

use Maybeway\Command\Command;
use Maybeway\Command\CommandConvention;
use Maybeway\Command\SimpleCommandBus\Exception\CommandHandlerIsNotCallable;
use Maybeway\Command\SimpleCommandBus\Exception\CommandHasNoCorrespondsHandler;
use Psr\Container\ContainerInterface;

/**
 * Class CommandBus
 * @package Maybeway\Command\SimpleCommandBus
 * @author Michal Koričanský <michal.koricansky@maybeway.com>
 */
class SimpleCommandBus
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * @var CommandConvention
	 */
	protected $commandConvention;


	public function __construct( ContainerInterface $container, CommandConvention $commandConvention )
	{
		$this->container = $container;
		$this->commandConvention = $commandConvention;
	}

	/**
	 * @param Command $command
	 * @throws CommandHandlerIsNotCallable
	 * @throws CommandHasNoCorrespondsHandler
	 */
	public function execute( Command $command )
	{
		$handler = $this->getCommandHandler( $command );

		if ( !is_callable( $handler ) && $handler instanceof CommandHandler ) {
			throw new CommandHandlerIsNotCallable( get_class( $handler ) );
		}

		$handler( $command );
	}

	/**
	 * @param Command $command
	 * @return mixed
	 * @throws CommandHasNoCorrespondsHandler
	 */
	protected function getCommandHandler( Command $command )
	{
		$handlerClass = $this->commandConvention->handlerName( $command );

		if ( !$this->container->has( $handlerClass ) )
		{
			throw new CommandHasNoCorrespondsHandler( $handlerClass );
		}

		return $this->container->get( $handlerClass );
	}
}